#!/bin/bash
# Copyright (C) Eclipse Foundation, Inc. and others. 
# 
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License v. 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
# 
# SPDX-License-Identifier: EPL-2.0

# TODO incorporate this script into the Maven build.
# 
# This script likely only runs on Wayne's desktop.

cd "${BASH_SOURCE%/*}"
mvn -f ../pom.xml clean process-resources
cp ../target/generated-docs/genai.html /gitroot/www.eclipse.org/projects/content/en_genai.html
